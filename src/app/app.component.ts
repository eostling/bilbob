import { Component } from '@angular/core';
import {math} from from "./app/classes/math";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent {
  title: string = 'Find Bilbo baggins';
  lat: number = 51.678418;
  lng: number = 7.809007;
}
