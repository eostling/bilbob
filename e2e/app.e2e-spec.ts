import { BilboBagPage } from './app.po';

describe('bilbo-bag App', function() {
  let page: BilboBagPage;

  beforeEach(() => {
    page = new BilboBagPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
